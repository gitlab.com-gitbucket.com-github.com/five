# VDR plugin language source file.
# Copyright (C) 2008 Martin Schirrmacher <vdr.neutrinoepg@schirrmacher.eu>
# This file is distributed under the same license as the Nordlicht's EPG-Plugin package.
#
#
msgid ""
msgstr ""
"Project-Id-Version: Neutrinoepg-Plugin 0.3.3\n"
"Report-Msgid-Bugs-To: <see README>\n"
"POT-Creation-Date: 2013-08-28 21:21+0200\n"
"PO-Revision-Date: 2008-03-30 19:58+0200\n"
"Last-Translator: \n"
"Language-Team: <nordlicht@martins-kabuff.de>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-15\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Button$Edit T."
msgstr "T. editieren"

msgid "Button$Delete T."
msgstr "T. l�schen"

msgid "Button$Search for repeats"
msgstr "Wiederholungen suchen"

msgid "no program info"
msgstr "keine Programminformation"

msgid "Can't switch channel!"
msgstr "Kann Kanal nicht wechseln!"

msgid "left/right"
msgstr "links/rechts"

msgid "prev/next"
msgstr "prev/next"

msgid "Blue"
msgstr "Blau"

msgid "Ok"
msgstr "Ok"

msgid "VDRSymbols"
msgstr "VDRSymbols"

msgid "Text"
msgstr "Text"

msgid "Percent"
msgstr "Prozent"

msgid "no filter"
msgstr "<kein filter>"

msgid "Behavior"
msgstr "Verhalten"

msgid "Step width (min)"
msgstr "Sprungweite (min)"

msgid "Favorite time"
msgstr "Favoriten Zeit"

msgid "Key to switch channel"
msgstr "Taste zum Umschalten"

msgid "Selected item centered"
msgstr "ausgew�hlten Kanal mittig"

msgid "Keys to switch channel group"
msgstr "Gruppenwechsel-Tasten"

msgid "Appearance"
msgstr "Erscheinungsbild"

msgid "Hide main menu entry"
msgstr "Eintrag im Hauptmen� verstecken"

msgid "Channel name width"
msgstr "Kanalnamen Spaltenbreite"

msgid "Keep display after switching"
msgstr "Anzeige nach Umschalten beibehalten"

msgid "Show channel numbers"
msgstr "Kanalnummern anzeigen"

msgid "Hide Groups at"
msgstr "Gruppen ausblenden ab Gruppe"

msgid "Hide encrypted channels"
msgstr "Verschl�sselte Kan�le ausblenden"

msgid "Hide radio channels"
msgstr "Radio-Kan�le ausblenden"

msgid "Progressbar modus"
msgstr "Progressbar Modus"

msgid "without group"
msgstr "ohne Gruppe"

msgid "Jump to"
msgstr "Springe zu"

#~ msgid "Progress as percent"
#~ msgstr "Fortschrittsanzeige als Prozent"
